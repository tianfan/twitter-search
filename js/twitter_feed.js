$(function(){
    var Tweet = Backbone.Model.extend({
        defaults: {
            text: "cool"
        }
    });
    
    var SearchQuery = Backbone.Model.extend({
        defaults: {
            text: "lumberjack"
        }
    });

    //the original collection that doesn't duplicate tweet replacement
    var TweetCollection = Backbone.Collection.extend({
        model: Tweet,
        url: '../api/get_tweet.php?query=' + getParameterByName('query'),
        parse: function(response) {
            var return_result =  response.statuses;

            //linkify the link in the text field
            $.each(return_result, function(index, value){
                return_result[index].text = linkify(value.text);
            });

            return return_result;
        },
    });

    //new collection holds the add function which that can be use in the TweetListView initilization
    var Tweets = Backbone.Collection.extend({
        model: Tweet,
        url: '../api/get_tweet.php?query=' + encodeURIComponent(getParameterByName('query')),
        parse: function(response) {
            var return_result =  response.statuses;

            //linkify the link in the text field
            $.each(return_result, function(index, value){
                return_result[index].text = linkify(value.text);
            });

            return return_result;
        },
        add: function(models, options) {
            var newModels = [];
            _.each(models, function(model) {
                if (typeof this.get(model.id) === "undefined") {
                    newModels.push(model);
                }
            }, this);
            return Backbone.Collection.prototype.add.call(this, newModels, options);
        }

    });

    var TweetListView = Backbone.View.extend({

        tagName: "ul",

        className: "tweets",
        initialize: function(options) {
            this.collection.bind("add", function(model) {

                var tweetListItemView = new TweetListItemView({
                    model: model
                });

                $(this.el).prepend(tweetListItemView.render().el);
            }, this);
        },


        render: function(eventName) {
            /*_.each(this.model.models, function (msg) {
                $(this.el).prepend(new TweetListItemView({model:msg}).render().el);
            }, this);
            */
            return this;
        }

    });

    var TweetListItemView = Backbone.View.extend({
        tagName:"li",
        template:_.template($('#tweet_item').html()),

        render:function (eventName) {
            $(this.el).html(this.template(this.model.toJSON()));
            return this;
        }
    });

    var TweetRouter = Backbone.Router.extend({

        routes: {
            "": "displayTweet",
            "search": "searchTweet"
        },

        displayTweet: function() {

            $('input#query').val(getParameterByName('query'));
            var tweetCollection = new Tweets([], {});

            var tweetListView = new TweetListView({collection : tweetCollection});
           
            var $loading = $('#loadingDiv').hide();
            $(document)
                  .ajaxStart(function () {
                          $loading.show();
                            })
              .ajaxStop(function () {
                      $loading.hide();
                        });
                    
            $('#tweet_list').html(tweetListView.render().el);
            var updateTweets = function() {
                tweetCollection.fetch({
                    add: true
                });
                setTimeout(updateTweets, 5000);
            };
            updateTweets(); 
        }

    });

    var tweetRouter = new TweetRouter();

    Backbone.history.start();
});

function linkify(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    return replacedText;
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

