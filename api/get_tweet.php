<?php

$token_json_url = "http://twitter.realpub.net/api/get_auth_token.php";

$token_json = file_get_contents($token_json_url);

$token_json_data = json_decode($token_json, TRUE);

//var_dump($token_json_data);

$token = $token_json_data['access_token'];


$opts = array(
  'http'=>array(
    'method'=>"GET",
    'header'=>"Host: api.twitter.com\r\n" .
              "User-Agent: twitter-realpub\r\n" . 
              "Authorization: Bearer " . $token . "\r\n"
  )
);

$context = stream_context_create($opts);

// Open the file using the HTTP headers set above
$file = file_get_contents('https://api.twitter.com/1.1/search/tweets.json?q=' . urlencode($_GET['query']), false, $context);


print_r($file);

/*
 * use curl is not working for some reason, but I have the code here if you are interested.
$headers = array(
    "GET /1.1/search/tweets.json?q=lumberjack HTTP/1.1",
    "Host: api.twitter.com",
    "User-Agent: twitter-realpub",
    "Authorization: Bearer " . $token,
    "Accept-Encoding: gzip",
    "Content-Length: 29"
);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://api.twitter.com/1.1/search/tweets.json?q=lumberjack');
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$content = curl_exec( $ch );

//var_dump($content);

if ($content === false)
{
    // throw new Exception('Curl error: ' . curl_error($crl));
    print_r('Curl error: ' . curl_error($ch));
}

curl_close( $ch );
print_r($content);

echo $content;
*/

?>
